# 1.0.0 (2021-08-31)


### Features

* an amazing feature that uninstall emcas ([10dd2b4](https://gitlab.com/smartnok/linux-base-collection/commit/10dd2b4781b086eab3b5a2b2dfc2c34993512268))
* update galaxy file ([013f8c1](https://gitlab.com/smartnok/linux-base-collection/commit/013f8c1af10b079f09d9e4f68cb2ad2baff5b2d1))
